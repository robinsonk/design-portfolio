// Next step - create about page and implement dark mode vs light 
// css and connect JS to make sure mode stays consistent


let darkMode = true;  
let miniUI = false;
let hamOpen = false;

function modeCheck() {
    console.log('Dark mode is on: ' + darkMode);
}

function toggleMode() {
    darkMode = darkMode;
    $('#modeToggle').change(function() {
        darkMode = !darkMode;
        modeCheck(darkMode);
        if ($('#modeToggle').is(':checked')) {
            toggleLight(darkMode);
        } else {
            toggleDark(darkMode)
        }
    }); 
};

function toggleLight(darkMode) {
    console.log('we are now in light mode')
    $('body, .cta-button, header, .slider, .moon, .work, .about').addClass('light');
    $('p, h1, h2, li, a, .h1-span').addClass('light-text');
    $('.work-header-side.desktop-side img').attr('src', './images/WORKHEADER-BLACK.png')
    $('.about-header-side.desktop-side img').attr('src', './images/about-hdr-wht.png')
    $('.work-header-side.side-mobile img').attr('src', './images/WORKHEADER-BLACK2x.png')
    $('.about-header-side.side-mobile img').attr('src', './images/about-hdr-wht2x.png')
    $('.dark-mode-logo').hide();
    $('.light-mode-logo').show();
    
    console.log(darkMode);
}

function toggleDark(darkMode) {
    console.log('we are back in dark mode')
    $('body, .cta-button, header, .slider, .moon, .work, .about').removeClass('light');
    $('p, h1, h2, li, a, .h1-span').removeClass('light-text')
    $('.work-header-side.desktop-side img').attr('src', './images/WORKHEADER-WHITE.png')
    $('.about-header-side.desktop-side img').attr('src', './images/about-hdr.png')
    $('.work-header-side.side-mobile img').attr('src', './images/WORKHEADER-WHITE2x.png')
    $('.about-header-side.side-mobile img').attr('src', './images/about-hdr2x.png')
    $('.light-mode-logo').hide();
    $('.dark-mode-logo').show();

    console.log(darkMode);
}

function navToggle() {
    miniUI = miniUI;
    $('#navToggle').click(function() {
        miniUI = !miniUI;
        console.log('nav toggle clicked');
        console.log(miniUI);

        if (miniUI === true) {
            $('#miniUL, .mini-nav, .mini-nav-toggle').addClass('show');
        } else {
            $('#miniUL, .mini-nav, .mini-nav-toggle').removeClass('show');
        }
        
    })
}

function modalWatch () {
    $('.modalToggle').click(function(e) {
        if ($(this).hasClass('mlModal') === true) {
            $('#mlModal').fadeIn("100");
            $('#mlModal').addClass('modal-open');
            console.log('the lodge')
        } else if ($(this).hasClass('modalmiscDesign') === true) {
            $('#modalmiscDesign').fadeIn("100");
            $('#modalmiscDesign').addClass('modal-open');
            console.log('miscDesign')
        } else if ($(this).hasClass('coinModal') === true) {
            $('#coinModal').fadeIn("100");
            $('#coinModal').addClass('modal-open');
            console.log('coin')
        } else if ($(this).hasClass('planModal') === true) {
            $('#planModal').fadeIn("100");
            $('#planModal').addClass('modal-open');
            console.log('planModal')
        }
        e.preventDefault();
        $('main, header, .mini-nav, nav').attr('aria-disabled', 'true');
        $('header, main, .mini-nav, nav').css('visibility', 'hidden');
        $('#work button, #work a, nav').hide();
        $('body').addClass('modal-open');
    })

    $('.modal-close, .modal-close-mobile').click(function(e) {
        e.preventDefault();
        $('.modal-container').removeClass('modal-open');
        $('body').removeClass('modal-open');
        $('header, main, .mini-nav, nav').css('visibility', 'visible');
        $('main, header, .mini-nav, nav').attr('aria-disabled', 'false');
        $('#work button, #work a, nav').show();
        $('.modal-container').fadeOut("slow");
    })
}

function mobileModal() {
    hamOpen = hamOpen;
    $('.hamburger').click(function() {
        hamOpen = !hamOpen;
        if (hamOpen === true) {
            $('.cs-nav-mobile').fadeIn();
            $('.hamburger').addClass('ham-open');
        } else {
            console.log('open ham clicked')
            $('.cs-nav-mobile').fadeOut("slow");
            $('.hamburger').removeClass('ham-open');
        }
    })
}

function modalNav() {
    $('#mlModal').scroll(function() {
        let hT = $('#mlScrollTrigger').offset().top,
            hH = $('#mlScrollTrigger').outerHeight(),
            wH = $(window).height(),
            wS = $(this).scrollTop();
        if (wS > (hT+hH-wH)){
            $('.modal-nav-li').fadeIn("slow");
            $('.modal-nav-li').css('opacity', 1);
        } else {
            $('.modal-nav-li').css('opacity', 0);
        }
    });

    $('#modalmiscDesign').scroll(function() {
        let hT = $('#scrollTriggermiscDesign').offset().top,
            hH = $('#scrollTriggermiscDesign').outerHeight(),
            wH = $(window).height(),
            wS = $(this).scrollTop();
        if (wS > (hT+hH-wH)){
            $('.modal-nav-li').fadeIn("slow");
            $('.modal-nav-li').css('opacity', 1);
        } else {
            $('.modal-nav-li').css('opacity', 0);
        }
    });

    $('#planModal').scroll(function() {
        let hT = $('.pdb-scroll-trigger').offset().top,
            hH = $('.pdb-scroll-trigger').outerHeight(),
            wH = $(window).height(),
            wS = $(this).scrollTop();
        if (wS > (hT+hH-wH)){
            $('.modal-nav-li').css('opacity', 1);
        } else {
            $('.modal-nav-li').css('opacity', 1);
        }
    });

    $('#coinModal').scroll(function() {
        let hT = $('#coinScrollTrigger').offset().top,
            hH = $('#coinScrollTrigger').outerHeight(),
            wH = $(window).height(),
            wS = $(this).scrollTop();
        if (wS > (hT+hH-wH)){
            $('.modal-nav-li').fadeIn("slow");
            $('.modal-nav-li').css('opacity', 1);
        } else {
            $('.modal-nav-li').css('opacity', 0);
        }
    });

    
}

function imgZoomModal() {
    $('img').on('click', function() {
        if ($(this).hasClass('modalToggle')) {
            return;
        }
    });
    // Get the modal
    let imgModal = document.getElementById('imgModalContainer');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    let img = document.getElementById('myImg');
    let modalImg = document.getElementById("img01");
    img.onclick = function(){
        modal.style.display = "block";
        modalImg.src = this.src;
        modalImg.alt = this.alt;
        captionText.innerHTML = this.alt;
    }


    // When the user clicks on <span> (x), close the modal
    imgModal.onclick = function() {
    img01.className += " out";
    setTimeout(function() {
        imgModal.style.display = "none";
        img01.className = "modal-content";
        }, 400);

    }
}


$(modeCheck);
$(toggleMode);
$(navToggle);
$(modalWatch);
$(mobileModal);
$(modalNav);
$(imgZoomModal);

